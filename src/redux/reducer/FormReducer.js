import {
  THEM_SINH_VIEN,
  XOA_SINH_VIEN,
  SUA_THONG_TIN,
} from "./constant/formConstant";
const initialState = {
  mangSinhVien: [
    {
      maSV: "001",
      hoTen: "Nguyen Van Nam",
      soDienThoai: "0867577849",
      email: "alice@gmail.com",
    },
  ],
  suaThongTin: {
    maSV: "",
    hoTen: "",
    soDienThoai: "",
    email: "",
  },
};

export const FormReducer = (state = initialState, action) => {
  switch (action.type) {
    case THEM_SINH_VIEN: {
      // Thêm dữ liệu sinh viên vào mangSinhVien
      let mangSVUpdate = [...state.mangSinhVien, action.sinhVien];
      state.mangSinhVien = mangSVUpdate;
      return { ...state };
      console.log(action);
    }
    case XOA_SINH_VIEN: {
      let mangSVUpdate = [...state.mangSinhVien];
      mangSVUpdate.splice(action.sinhVien, 1);
      state.mangSinhVien = mangSVUpdate;
      return { ...state };
    }
    case SUA_THONG_TIN: {
      return { ...state, suaThongTin: action.sinhVien };
    }
    default: {
      return { ...state };
    }
  }
};
