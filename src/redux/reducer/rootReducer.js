import { combineReducers } from "redux";
import { FormReducer } from "./FormReducer";

export const rootReducer_Form = combineReducers({
  FormReducer,
});
