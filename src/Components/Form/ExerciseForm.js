import React, { Component } from "react";
import Form from "./Form";
import Table from "./Table";

export default class ExerciseForm extends Component {
  render() {
    return (
      <div>
        <h3>Exercise Form</h3>
        <Form />
        <Table />
      </div>
    );
  }
}
