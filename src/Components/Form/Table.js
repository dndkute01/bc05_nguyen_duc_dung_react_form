import React, { Component } from "react";
import { connect } from "react-redux";
import { XOA_SINH_VIEN } from "../../redux/reducer/constant/formConstant";
import { FormReducer } from "./../../redux/reducer/FormReducer";
import { SUA_THONG_TIN } from "./../../redux/reducer/constant/formConstant";

class Table extends Component {
  renderSinhVien = () => {
    return this.props.mangSinhvien.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.maSV}</td>
          <td>{item.hoTen}</td>
          <td>{item.soDienThoai}</td>
          <td>{item.email}</td>
          <td>
            <button
              onClick={() => {
                this.props.xoaSinhVien(index);
              }}
              className="btn btn-outline-danger"
            >
              Xóa
            </button>
            <button
              onClick={() => {
                this.props.suaThongTin(item);
              }}
              className="btn btn-outline-warning"
            >
              Sửa
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    console.log(this.props.mangSinhvien);
    return (
      <div className="container">
        <table className="table">
          <thead>
            <tr className="bg-dark text-white">
              <th>Mã SV</th>
              <th>Họ tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>{this.renderSinhVien()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    mangSinhvien: state.FormReducer.mangSinhVien,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    xoaSinhVien: (sinhVien) => {
      let action = {
        type: XOA_SINH_VIEN,
        sinhVien,
      };

      dispatch(action);
    },
    suaThongTin: (sinhVien) => {
      let action = {
        type: SUA_THONG_TIN,
        sinhVien,
      };
      console.log(action.sinhVien);
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Table);
