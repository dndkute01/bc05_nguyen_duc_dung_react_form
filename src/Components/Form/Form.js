import React, { Component } from "react";
import { connect } from "react-redux";
import { THEM_SINH_VIEN } from "./../../redux/reducer/constant/formConstant";

class Form extends Component {
  state = {
    values: {
      maSV: "",
      hoTen: "",
      soDienThoai: "",
      email: "",
    },
    errors: {
      maSV: "",
      hoTen: "",
      soDienThoai: "",
      email: "",
    },
    valid: false,
  };

  handleChange = (e) => {
    let tagInput = e.target;

    let { name, value, type, pattern } = tagInput;

    let errorMessage = "";

    if (value.trim() == "") {
      // Kiểm tra rỗng
      errorMessage = name + "không được bỏ trống";
    }

    // check email
    if (
      type === "email" ||
      name === "soDienThoai" ||
      name === "maSV" ||
      name === "hoTen"
    ) {
      let regex = new RegExp(pattern);
      if (!regex.test(value)) {
        errorMessage = name + "không đúng định dạng";
      }
    }

    // Cập nhật giá trị cho state
    let values = { ...this.state.values, [name]: value };
    let errors = { ...this.state.errors, [name]: errorMessage };

    this.setState(
      {
        values: values,
        errors: errors,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  handleSubmit = (e) => {
    e.preventDefault(); //Cản sự kiện submit reload trang của browser
    this.props.themSinhVien(this.state.values);
  };

  renderButtonSubmit = () => {
    let valid = true;
    for (let key in this.state.errors) {
      if (this.state.errors[key] != "") {
        valid = false;
      }
    }
    for (let key in this.state.values) {
      if (this.state.values[key] == "") {
        valid = false;
      }
    }

    if (valid) {
      return (
        <button type="submit" className="btn btn-outline-success">
          Thêm sinh viên
        </button>
      );
    } else {
      return (
        <button type="submit" className="btn btn-outline-success" disabled>
          Thêm sinh viên
        </button>
      );
    }
  };

  render() {
    return (
      <div className="container">
        <div className="card text-left">
          <div className="card-header bg-dark text-white">
            <h3>Thông tin sinh viên</h3>
          </div>
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="form-group col-6">
                  <input
                    pattern="^[1-9][0-9]*$"
                    className="form-control"
                    name="maSV"
                    placeholder="Mã SV"
                    value={this.state.values.maSV}
                    onChange={this.handleChange}
                  />
                  <p className="text-danger">{this.state.errors.maSV}</p>
                </div>
                <div className="form-group col-6">
                  <input
                    pattern="^[a-zA-Z]+$"
                    className="form-control"
                    name="hoTen"
                    placeholder="Họ tên "
                    value={this.state.values.hoTen}
                    onChange={this.handleChange}
                  />
                  <p className="text-danger">{this.state.errors.hoTen}</p>
                </div>
              </div>
              <div className="row">
                <div className="form-group col-6">
                  <input
                    type="text"
                    pattern="^[1-9][0-9]*$"
                    className="form-control"
                    name="soDienThoai"
                    placeholder="Số điện thoại"
                    value={this.state.values.soDienThoai}
                    onChange={this.handleChange}
                  />
                  <p className="text-danger">{this.state.errors.soDienThoai}</p>
                </div>
                <div className="form-group col-6">
                  <input
                    type="email"
                    pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
                    className="form-control"
                    name="email"
                    placeholder="Email"
                    value={this.state.values.email}
                    onChange={this.handleChange}
                  />
                  <p className="text-danger">{this.state.errors.email}</p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 text-right">
                  {this.renderButtonSubmit()}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    suaThongTin: state.FormReducer.suaThongTin,
  };
};

let mapDispathToProps = (dispatch) => {
  return {
    themSinhVien: (sinhVien) => {
      let action = {
        type: THEM_SINH_VIEN,
        sinhVien,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispathToProps)(Form);
