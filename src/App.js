import logo from "./logo.svg";
import "./App.css";
import ExerciseForm from "./Components/Form/ExerciseForm";
import Table from "./Components/Form/Table";

function App() {
  return (
    <div className="App">
      <ExerciseForm />
    </div>
  );
}

export default App;
